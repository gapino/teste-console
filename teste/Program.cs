﻿using ShortWayLibrary;
using System;

namespace teste
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                if (args.Length <= 0)
                {
                    throw new Exception("No such file or directory .csv");
                }

                FileHelper fileHelper = new FileHelper(args[0]);

                var existFile = fileHelper.ExistArquivo();

                if (existFile.IsSuccess)
                {
                    var linesFiles = fileHelper.GetLinesFile();

                    if (linesFiles.Length <= 0)
                    {
                        throw new Exception("No Records");
                    }

                    UtilLibrary controller = new UtilLibrary(linesFiles);
                    controller.DoNodes();

                    while (true)
                    {
                        Console.WriteLine("please enter the route:");

                        var routes = Console.ReadLine();
                        var citys = routes.Split("-");
                        var start = citys[0].Trim();
                        var end = citys[1].Trim();

                        Console.WriteLine($"best route: {controller.PrintResult(start.Trim(), end.Trim())}");
                    }
                }
                else {
                    throw new Exception("No such file or directory .csv");
                }
            }
            catch (Exception message)
            {
                Console.WriteLine(message.Message);
            }
        }
    }
}
