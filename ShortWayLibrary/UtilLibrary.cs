﻿using System;
using System.Collections.Generic;

namespace ShortWayLibrary
{
    public class UtilLibrary
    {
        public string[] Routes { get; set; }

        public List<string> Nodes { get; set; }

        public int[,] MatrixDistancia { get; set; }

        public int[,] MatrixLocation { get; set; }


        private const int _infinity = 1000000;

        public int Size { get; set; }

        public UtilLibrary(string[] routes)
        {
            Routes = routes;
            Size = Routes.Length;
            Nodes = new List<string>();
        }

        public void DoNodes()
        {

            for (int i = 0; i < Size; i++)
            {
                var linea = Routes[i].Split(',');

                if (linea.Length != 3)
                {
                    throw new Exception("No record. Verify file");
                }

                if (!Nodes.Contains(linea[0]))
                {
                    Nodes.Add(linea[0]);
                }

                if (!Nodes.Contains(linea[1]))
                {
                    Nodes.Add(linea[1]);
                }
            }

            LoadMatrix();
            FloydWarshall();
        }

        public void LoadMatrix()
        {
            var size = Nodes.Count;
            MatrixDistancia = new int[size, size];
            MatrixLocation = new int[size, size];

            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    if (i == j)
                    {
                        MatrixDistancia[i, j] = 0;
                    }
                    else
                    {
                        MatrixDistancia[i, j] = _infinity;
                        MatrixLocation[i, j] = j + 1;
                    }
                }
            }

            for (int k = 0; k < size; k++)
            {
                for (int h = 0; h < Size; h++)
                {
                    var linea = Routes[h].Split(',');

                    if (Nodes[k].Equals(linea[0]))
                    {
                        var secondnode = Nodes.IndexOf(linea[1]);
                        MatrixDistancia[k, secondnode] = Int32.Parse(linea[2]);
                    }
                }
            }
        }

        public string PrintResult(string start, string end)
        {
            string path = "";

            for (int i = 0; i < MatrixLocation.GetLength(0); i++)
            {
                for (int j = 0; j < MatrixLocation.GetLength(1); j++)
                {
                    if (i != j)
                    {
                        int u = i + 1;
                        int v = j + 1;

                        if (Nodes[i].Equals(start, StringComparison.OrdinalIgnoreCase) &&
                            Nodes[j].Equals(end, StringComparison.OrdinalIgnoreCase) &&
                            MatrixDistancia[i, j] < 1000000)
                        {
                            path = string.Format("{0}", Nodes[i]);
                            do
                            {
                                u = MatrixLocation[u - 1, v - 1];
                                path += " - " + Nodes[u - 1];
                            } while (u != v);
                            path += $" > ${MatrixDistancia[i, j]}";
                        }
                    }
                }
            }
            return String.IsNullOrEmpty(path) ? "No Route" : path;
        }

        public void FloydWarshall()
        {
            var size = Nodes.Count;

            for (int k = 0; k < size; ++k)
            {
                for (int i = 0; i < size; ++i)
                {
                    for (int j = 0; j < size; ++j)
                    {
                        if (MatrixDistancia[i, k] + MatrixDistancia[k, j] < MatrixDistancia[i, j])
                        {
                            MatrixDistancia[i, j] = MatrixDistancia[i, k] + MatrixDistancia[k, j];
                            MatrixLocation[i, j] = MatrixLocation[i, k];
                        }

                    }
                }
            }
        }
    }
}
