﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShortWayLibrary
{
    public class Result
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
    }
}
