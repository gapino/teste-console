﻿using System.IO;

namespace ShortWayLibrary
{
    public class FileHelper
    {
        public string Path { get; set; }

        public FileHelper(string path)
        {
            Path = path;
        }

        public Result ExistArquivo()
        {
            return File.Exists(Path) ? new Result { IsSuccess = true, Message = "O arquivo existe" } :
                new Result { IsSuccess = false, Message = "O arquivo não existe" };
        }

        public string[] GetLinesFile()
        {
            return ExistArquivo().IsSuccess ? File.ReadAllLines(Path) : null;
        }
    }
}
